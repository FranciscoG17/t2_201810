package model.vo;

/**
 * Representation of a Service object
 */
public class Service implements Comparable<Service> {

	private String tripId;
	
	private String taxiId;
	
	private int tripSeconds;
	
	private double tripMiles;
	
	private double tripTotal;
	
	private int dropOffCommunityArea;
	
	public Service(String pTripId, String pTaxiId, int pTripSec, double pTripMiles, double pTripTotal, int pDropOffCommunityArea)
	{
		tripId = pTripId;
		taxiId = pTaxiId;
		tripSeconds = pTripSec;
		tripMiles = pTripMiles;
		tripTotal = pTripTotal;
		dropOffCommunityArea = pDropOffCommunityArea;
	}
	
	/**
	 * @return id - Trip_id
	 */
	public String getTripId()
	{
		return tripId;
	}	
	
	/**
	 * @return id - Taxi_id
	 */
	public String getTaxiId() 
	{
		return taxiId;
	}	
	
	/**
	 * @return time - Time of the trip in seconds.
	 */
	public int getTripSeconds() 
	{
		return tripSeconds;
	}

	/**
	 * @return miles - Distance of the trip in miles.
	 */
	public double getTripMiles() 
	{
		return tripMiles;
	}
	
	/**
	 * @return total - Total cost of the trip
	 */
	public double getTripTotal()
	{
		return tripTotal;
	}
	
	public int getDropoffCommunityArea()
	{
		return dropOffCommunityArea;
	}

	@Override
	public int compareTo(Service o) 
	{
		int comparation = tripId.compareTo(o.tripId);
		if(comparation < 0)
		{
			return -1;
		}
		else if(comparation > 0)
		{
			return 1;
		}
		else
		{
			return 0;			
		}
	}
}