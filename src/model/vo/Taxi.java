package model.vo;

import model.data_structures.DoubleLinkedList;

/**
 * Representation of a taxi object
 */
public class Taxi implements Comparable<Taxi>{

	private String id;

	private String company;

	private DoubleLinkedList<Service> services;

	public Taxi(String pCompany, String pId)
	{
		company = pCompany;
		id = pId;
		services = new DoubleLinkedList<Service>();
	}
	public String getTaxiId() 
	{
		return id;
	}
	public String getCompany() 
	{
		return company;
	}

	public DoubleLinkedList<Service> getListOfServices()
	{
		return services;
	}

	@Override
	public int compareTo(Taxi o) 
	{
		int comparador = id.compareTo(o.id);
		if(comparador < 0)
		{
			return -1;
		}
		else if(comparador > 0)
		{
			return 1;
		}
		else
		{
			return 0;			
		}
	}	
}