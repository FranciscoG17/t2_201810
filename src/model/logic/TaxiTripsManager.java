package model.logic;

import java.io.FileNotFoundException;
import java.io.FileReader;

import com.google.gson.JsonArray;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import api.ITaxiTripsManager;
import model.vo.Taxi;
import model.vo.Service;
import model.data_structures.DoubleLinkedList;

public class TaxiTripsManager implements ITaxiTripsManager {

	// TODO Constantes.
	public static final String TRIP_ID = "trip_id";
	public static final String TAXI_ID ="taxi_id";
	public static final String TRIP_SECONDS ="trip_seconds";
	public static final String TRIP_MILES ="trip_miles";
	public static final String TRIP_TOTAL ="trip_seconds";
	public static final String DOFF_AREA ="dropoff_community_area";

	private DoubleLinkedList<Taxi> taxis;

	public TaxiTripsManager()
	{
		taxis = new DoubleLinkedList<Taxi>();
	}

	public void loadServices (String serviceFile) 
	{
		JsonParser parser = new JsonParser();

		try 
		{
			/* Cargar todos los JsonObject (servicio) definidos en un JsonArray en el archivo */
			JsonArray arr= (JsonArray) parser.parse(new FileReader(serviceFile));

			/* Tratar cada JsonObject (Servicio taxi) del JsonArray */
			for (int i = 0; arr != null && i < arr.size(); i++)
			{
				JsonObject obj= (JsonObject) arr.get(i);
				/* Mostrar un JsonObject (Servicio taxi) */

				// TODO Pedir datos.

				String company_name = "NN";
				if(obj.get("company") != null)
				{ company_name = obj.get("company").getAsString(); }

				String taxi_id = "NaN";
				if ( obj.get("taxi_id") != null )
				{ taxi_id = obj.get("taxi_id").getAsString(); }

				String trip_id = "NaN";			
				if ( obj.get(TRIP_ID) != null )
				{ trip_id = obj.get(TRIP_ID).getAsString(); }
				
				int drop_off_area = -17;			
				if ( obj.get(DOFF_AREA) != null )
				 drop_off_area = obj.get(DOFF_AREA).getAsInt(); 
				
				int trip_sec = 0;
				if(obj.get(TRIP_SECONDS)!= null)
				trip_sec = obj.get(TRIP_SECONDS).getAsInt();
				
				double trip_miles = 0.0;
				if(obj.get(TRIP_MILES) != null)
				{
					trip_miles = obj.get(TRIP_MILES).getAsDouble();
				}
				double trip_total = 0.0;
				if(obj.get(TRIP_TOTAL)!= null)
					trip_total = obj.get(TRIP_TOTAL).getAsDouble();
				
				Taxi tx1 = new Taxi(company_name, taxi_id);
				if(!taxis.add(tx1))
				{
					tx1.getListOfServices().add(new Service(trip_id, taxi_id, trip_sec, trip_miles, trip_total, drop_off_area));
				}
				else
				{
					tx1.getListOfServices().add(new Service(trip_id, taxi_id, trip_sec, trip_miles, trip_total, drop_off_area));
				}
				
			}
		}
		catch (JsonIOException e1 ) 
		{
			e1.printStackTrace();
		}
		catch (JsonSyntaxException e2) 
		{
			e2.printStackTrace();
		}
		catch (FileNotFoundException e3) 
		{
			e3.printStackTrace();
		}
		System.out.println("Inside loadServices with " + serviceFile);
	}
	public DoubleLinkedList<Taxi> getTaxisOfCompany(String company) 
	{
		DoubleLinkedList<Taxi> listica = new DoubleLinkedList<Taxi>();
		System.out.println("Los id de los taxis son: \n ");
		for (int i = 0; i < taxis.size(); i++)
		{
			if(taxis.get(i).getCompany().equals(company))
			{
				listica.add(taxis.get(i));
				System.out.println(taxis.get(i).getTaxiId());
			}
		}
		
		return listica;
	}
	@Override
	public DoubleLinkedList<Service> getTaxiServicesToCommunityArea(int communityArea) 
	{
		DoubleLinkedList<Service> listOfServicesInArea = new DoubleLinkedList<Service>();
		System.out.println("Los id de los servicios son \n");
		for (int i = 0; i < taxis.size(); i++) 
		{
			for (int j = 0; j < taxis.get(i).getListOfServices().size(); j++) 
			{
				 
				if( taxis.get(i).getListOfServices().get(j).getDropoffCommunityArea() == communityArea)
				{
					listOfServicesInArea.add( taxis.get(i).getListOfServices().get(j));
					System.out.println(taxis.get(i).getListOfServices().get(j).getTripId());
				}
			}
		}
		return listOfServicesInArea;
	}


}
