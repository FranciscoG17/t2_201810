package model.data_structures;

public class DoubleLinkedList<T extends Comparable<T>> extends AbstractList<T>
{
	private DoubleNode<T> actualNode;
	
	 /**
     * Construye una lista vacia
     * <b>post:< /b> se ha inicializado el primer nodo en null
     */
	public DoubleLinkedList() 
	{
		firstNode = null;
		actualNode = null;
		size = 0;
	}
	
	/**
     * Se construye una nueva lista cuyo primer nodo  guardar� al elemento que llega por par�mentro
     * @param nFirst el elemento a guardar en el primer nodo
     * @throws NullPointerException si el elemento recibido es nulo
     */
	public DoubleLinkedList(T nFirst)
	{
		if(nFirst == null)
		{
			throw new NullPointerException("Se recibe un elemento nulo");
		}
		firstNode = new DoubleNode<T>(nFirst);
		actualNode = null;
		size = 1;
	}
	
	/**
     * Agrega un elemento al final de la lista
     * Un elemento no se agrega si la lista ya tiene un elemento con el mismo id.
     * Se actualiza la cantidad de elementos.
     * @param e el elemento que se desea agregar.
     * @return true en caso que se agregue el elemento o false en caso contrario. 
     * @throws NullPointerException si el elemento es nulo
     */
	@Override
	public boolean add(T element) 
	{
		boolean agregado = false;
		if(element == null)
		{
			throw new NullPointerException();
		}
		if(!this.contains(element))
		{
			if(this.isEmpty())
			{
				this.firstNode = new DoubleNode<T>(element);
				this.size++;
				agregado = true;
			}
			else
			{
				DoubleNode<T> n = (DoubleNode<T>) this.firstNode;
				while(n.getNextNode() != null)
				{
					n = (DoubleNode<T>) n.getNextNode();
				}
				
				DoubleNode<T> nuevo = new DoubleNode<T>(element);
				nuevo.changeNext(n.getNextNode());
				n.changeNext(nuevo);	
				if(nuevo.getNextNode() != null)
				{
					((DoubleNode<T>)nuevo.getNextNode()).changePrevious(nuevo);
				}
				nuevo.changePrevious(n);
				agregado = true;
				this.size++;
			}
		}
		return agregado;		
	}

	@Override
	public boolean delete(T o) 
	{
		boolean elimino = false;
		boolean encontro = false;
		
		DoubleNode<T> n = (DoubleNode<T>) this.firstNode;
		while(n != null && ! encontro)
		{
			if(n.getElement().compareTo((T)o) == 0)
			{
				encontro = true;
			}
			else
			{		
				n = (DoubleNode<T>) n.getNextNode();
			}
		}
		
		if(encontro)
		{
			DoubleNode<T> anterior = n.getPrevious();
			if(anterior == null)
			{
				this.firstNode = n.getNextNode();
				if(firstNode != null)
				{
					((DoubleNode<T>)this.firstNode).changePrevious(null);
				}
			}
			else 
			{
				if (n.getNextNode() != null)
				{
					anterior.changeNext(n.getNextNode());
					((DoubleNode<T>)n.getNextNode()).changePrevious(anterior);
				}
				else
				{
					anterior.changeNext(n.getNextNode());
				}
			}
			elimino = true;
			this.size--;
		}
		return elimino;	
	}

	@Override
	public T get(T o) 
	{
		DoubleNode<T> n = (DoubleNode<T>)this.firstNode;
		T searched = null;
		boolean found = false;
		if(n != null)
		{
			while(n != null && !found)
			{
				if(n.getElement().compareTo((T)o) == 0)
				{
					found = true;
					searched = n.getElement();
				}
				else
				{
					n = (DoubleNode<T>) n.getNextNode();
				}
			}
		}
		return searched;
	}

	@Override
	public void listing() 
	{
		actualNode = (DoubleNode<T>) this.firstNode;
		
	}
	@Override
	public T getCurrent() 
	{
		return actualNode.getElement();
	}

	@Override
	public boolean next() 
	{
		boolean exists = false;
		if(actualNode != null)
		{
			exists = true;
			actualNode = (DoubleNode<T>) actualNode.getNextNode();
		}
		return exists;
	}
	public boolean previous()
	{
		boolean exists = false;
		if(actualNode != null)
		{
			exists = true;
			actualNode = actualNode.getPrevious();
		}
		return exists;
	}

}
