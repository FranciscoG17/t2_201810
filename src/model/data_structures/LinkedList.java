package model.data_structures;

public interface LinkedList <T extends Comparable<T>>  
{
	public boolean add(T element);
	
	public boolean delete(T o);
	public T get(T o);
	public int size();
	public T get (int index);
	public void listing();
	public T getCurrent();
	public boolean next();
	
}
