package model.data_structures;

public class Queues<T> {

	private Node<T> firstNode, lastNode;
	public void enqueue (T element)
	{
		Node<T> oldLast = lastNode;
		lastNode = new Node<T>(element);
		lastNode.next = null;
		if(isEmpty())
		{
			firstNode = lastNode;
		}
		else
		{
			oldLast.next = lastNode;
		}

	}
	public Node<T> dequeue()
	{
		Node<T> temp = firstNode;
		firstNode = firstNode.next;
		if(isEmpty())
			lastNode = null;
		return temp;
	}
	public boolean isEmpty()
	{
		return firstNode == null;

	}
}
