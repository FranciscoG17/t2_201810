package model.data_structures;

public class Node<T> {
	
	protected T element;
	
	protected Node<T> next;
	
	protected Node<T> previous;

	
	public Node(T pElement)
	{
		this.element = pElement;
	}
	public void createNext(T pNext)
	{
		this.next = new Node<T>(pNext);
	}
	public void createPrevious(T pPrevious)
	{
		this.previous = new Node<T>(pPrevious);
	}
	public T getActualElement()
	{
		return this.element;
	}
	public void changeNext(Node pNext)
	{
		this.next = pNext;
	}
	public void changePrevious( Node pPrevious)
	{
		this.previous = pPrevious;
	}
	public void setElement(T pElement)
	{
		this.element = pElement;
	}
	public int getIdentificator()
	{
		return this.element.hashCode();
	}
	public Node<T> getNext()
	{
		return this.next;
	}
	public Node<T> getPrevious()
	{
		return this.previous;
	}
	public boolean hasNext()
	{
		return next==null;
	}
	
	
	
	
	

}
