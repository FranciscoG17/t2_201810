package model.data_structures;


public class ListaDobleEncadenada <T extends Comparable<T>> implements ILinkedList<T> {

	private Node<T> firstNode;

	private int cantidadElementos;
	public ListaDobleEncadenada()
	{
		try
		{
			firstNode = null;
			cantidadElementos = 0;
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage()+"KAKA");
		}
			//System.out.println("KAKA");
		
	}
	public ListaDobleEncadenada(T pElement)
	{
		firstNode = new Node<T>(pElement);
		cantidadElementos = 1;
		//System.out.println("KAKA");
	}
	public void add(T pElement) 
	{
		if(pElement == null)
		{
			throw new NullPointerException();
		}

		if(!exists(pElement))
		{
			if(firstNode == null)
			{
				firstNode = new Node<T>(pElement);
				cantidadElementos++;
			}
			{
				Node<T> actual = firstNode;
				while(actual.next != null)
				{
					actual = actual.next;
				}

				Node<T> newl = new Node<T>(pElement);
				newl.changeNext(actual.next);
				actual.changeNext(newl);	
				if(newl.next != null)
				{
					newl.next.changePrevious(newl);
				}
				newl.changePrevious(actual);
				cantidadElementos++;
			}
		}
	}
	@Override
	public void delete(T element)
	{
		if(exists(element))
		{
			if(element.hashCode()== firstNode.hashCode())
			{
				firstNode = firstNode.next;
				cantidadElementos--;
				firstNode = null;
			}
			else
			{
				Node<T> previous = null;
				Node<T> actual = firstNode;
				while(actual != null && ! (actual.hashCode() == element.hashCode()))
				{
					previous = actual;
					actual = actual.next;
				}
				previous.changeNext(actual.next);
				actual.next.changePrevious(previous);
				cantidadElementos--;
			}
		}

		// TODO Auto-generated method stub
	}
	@Override
	public int size() {
		// TODO Auto-generated method stub
		return cantidadElementos;
	}

	@Override
	public T get(int position) {
		// TODO Auto-generated method stub
		Node<T> actual = firstNode;
		int contador = 0;
		while(actual.hasNext())
		{
			if(position == contador)
			{
				return (T) actual;
			}
			actual = actual.next;
			contador++;
		}
		return null;
	}

	@Override
	public void listing() 
	{
		Node<T> actual = firstNode;
		while(actual != null)
		{
			actual = actual.next;
		}
		// TODO Auto-generated method stub
	}

	@Override
	public T getCurrent()
	{
		return null;
		// TODO Auto-generated method bstub
		
	}
	@Override
	public void addIn(T elemento, int i)
	{
		if(elemento == null)
		{
			throw new NullPointerException( );
		}
		Node<T> added = new Node<T>( elemento );
		try
		{
			if(!exists( elemento ))
			{

				if(i == 0)
				{
					added.changeNext(firstNode);
					firstNode.changePrevious(added);
					firstNode = added;
					cantidadElementos++;
				}
				else
				{
					Node<T> actual = (Node<T>) firstNode;
					int posActual = 0;
					while( posActual < (i-1) && actual != null )
					{
						posActual++;
						actual =  actual.next;
					}
					if(posActual != (i-1))
					{
						throw new IndexOutOfBoundsException( );
					}
					added.changeNext( actual.next);
					added.changePrevious(actual);
					actual.changeNext( added );
					actual.next.changePrevious(added);
					cantidadElementos++;
				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		// TODO Auto-generated method stub

	}
	public boolean exists(T pElement) 
	{
		boolean existy = false;
		if(pElement != null)
		{
			Node<T> actual = firstNode;
			while(actual != null && !existy)
			{
				if(actual.getActualElement().hashCode() == pElement.hashCode())
				{
					existy = true;
				}
			}
		}
		return existy;
	}
	
}
