package model.data_structures;

public class Stack<T> {

	private Node<T> firstNode;

	public void push(T pElement)
	{
		Node<T> oldFirst = firstNode;
		firstNode = new Node<T>(pElement);
		firstNode.next = oldFirst;

	}
	public Node<T> pop()
	{
		Node<T> temporal = firstNode;
		firstNode = firstNode.next;
		return temporal;
	}
	public boolean isEmpty()
	{
		return firstNode == null;
	}
}
